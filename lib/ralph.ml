open Extensions

module type Data = sig
  type t

  val default : t

  val hash_one : t -> int

  val hash_two : int -> int -> int
end

module type T = sig
  type t

  type leaf_t

  val root_hash : t -> int

  val make : int -> leaf_t -> t

  val init_list : leaf_t list -> t

  val modify : int -> (leaf_t -> leaf_t) -> t -> t

  val insert : leaf_t -> t -> t

  val proof_tree : int -> t -> int list

  val mem : leaf_t -> int list -> int -> bool
end

module MerkleTree (D : Data) = struct
  type leaf_t = D.t

  type hash_t = int
  (** hash type is separated from int to prevent type errors *)

  (** The root is parameterized by the depth of the tree *)
  type t =
    | Leaf of hash_t * D.t
    | Node of hash_t * t * t
    | Root of int * hash_t * t * t

  let root_hash = function
    | Root (_, h, _, _) -> h
    | _ -> failwith "Users should not have access to non-root trees"

  let hash_of_t = function
    | Leaf (x, _) | Node (x, _, _) | Root (_, x, _, _) -> x

  let rec depth = function
    | Leaf _ -> 0
    | Node (_, left, _) -> succ @@ depth left
    | Root (d, _, _, _) -> d

  (** This function creates a single data node, not a full 1-depth tree *)
  let singleton x = Leaf (D.hash_one x, x)

  let hash_two x y = D.hash_two (hash_of_t x) (hash_of_t y)

  let pair x y = Node (hash_two x y, x, y)

  (** Update the hash value of a node, assuming at least one of the hashes
   *  of the sons has been changed *)
  let update_node t =
    match t with
    | Leaf (_, content) -> singleton content
    | Node (_, left, right) -> pair left right
    | Root (d, _, left, right) -> Root (d, hash_two left right, left, right)

  let make final_depth init =
    if final_depth < 1 then
      raise @@ Invalid_argument "Depth too small in MerkleTree.make";
    let rec aux current_depth left right =
      if current_depth = final_depth then
        Root (final_depth, hash_two left right, left, right)
      else
        let node = pair left right in
        aux (succ current_depth) node node
    in
    aux 1 (singleton init) (singleton init)

  let init_list elements =
    let rec aux depth acc l =
      match (acc, l) with
      | [ x ], [] -> (
          match x with
          | Node (h, left, right) -> Root (depth, h, left, right)
          | _ -> failwith "Logic error in MerkleTree.init_list")
      | _, [] -> aux (succ depth) [] (List.rev acc)
      | _, x :: y :: xs -> aux depth (pair x y :: acc) xs
      | _, [ _ ] -> raise @@ Invalid_argument "Given list has length < 2"
    in
    elements |> List.pad_2exp D.default |> List.map singleton |> aux 1 []

  let modify index modifier t =
    let d = depth t in
    if index < 0 || index >= 2 lsl (d - 1) then
      raise @@ Invalid_argument "Index out of bounds in MerkleTree.modify";
    (* Create the path to the leaf to be modified.
       a 'true' in the result list means going left. *)
    let rec choice_list n acc =
      if n = d then acc
      else choice_list (succ n) (((index lsr n) mod 2 = 0) :: acc)
    in
    let rec aux choices t =
      match (choices, t) with
      | [], Leaf (_, content) -> singleton @@ modifier content
      | choice :: xs, Node (h, left, right) ->
          update_node
          @@
          if choice then Node (h, aux xs left, right)
          else Node (h, left, aux xs right)
      | choice :: xs, Root (d, h, left, right) ->
          update_node
          @@
          if choice then Root (d, h, aux xs left, right)
          else Root (d, h, left, aux xs right)
      | _ -> failwith "Logic error in MerkleTree.modify"
    in
    aux (choice_list 0 []) t

  let insert elem t =
    match t with
    | Root (d, hl, ll, rl) -> (
        match make d elem with
        | Root (_, hr, lr, rr) ->
            Root (succ d, D.hash_two hl hr, Node (hl, ll, rl), Node (hr, lr, rr))
        | _ -> failwith "Impossible result from MerkleTree.make")
    | _ -> failwith "Users should not have access to non-root trees"

  let proof_tree index t =
    let d = depth t in
    if index < 0 || index >= 2 lsl (d - 1) then
      raise @@ Invalid_argument "Index out of bounds in MerkleTree.proof_tree";

    let rec aux current_depth index acc t =
      match t with
      | Leaf _ -> acc
      | Node (_, left, right) ->
          let mid_point = max 1 (2 lsl (current_depth - 2)) in
          if index < mid_point then
            aux (pred current_depth) index (hash_of_t right :: acc) left
          else
            aux (pred current_depth) (index - mid_point) (hash_of_t left :: acc)
              right
      | Root (_, _, left, right) ->
          let mid_point = max 1 (2 lsl (current_depth - 2)) in
          if index < mid_point then
            aux (pred current_depth) index (hash_of_t right :: acc) left
          else
            aux (pred current_depth) (index - mid_point) (hash_of_t left :: acc)
              right
    in
    aux d index [] t

  let mem element proof root =
    let rec aux h proof =
      match proof with [] -> h = root | x :: xs -> aux (D.hash_two h x) xs
    in
    aux (D.hash_one element) proof
end
