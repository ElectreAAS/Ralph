(** Maybe apply f to x *)
let ( @@? ) f x = match x with Some x -> f x | None -> None

(** Repeatedly apply the same function to an initial argument *)
let rec repeat (n : int) (f : 'a -> 'a) (x : 'a) : 'a =
  match n with 0 -> x | 1 -> f x | n -> repeat (n - 1) f (f x)

(** Repeatedly apply the same function to an argument until two successive calls return the same value *)
let rec repeatedly f x =
  let res = f x in
  if res = x then x else repeatedly f res

(** Computes the smallest power of 2 above or equal to x *)
let nearest_2exp_up x =
  let rec aux b = if b >= x then b else aux (b lsl 1) in
  aux 1

module List = struct
  include List

  (** Pad list 'l' up to a length that is a power of 2, using filler element 'el' *)
  let pad_2exp el l =
    let rec aux len acc l =
      match l with
      | [] ->
          let next = nearest_2exp_up len in
          repeat (next - len) (fun xs -> el :: xs) acc
      | x :: xs -> aux (len + 1) (x :: acc) xs
    in
    List.rev @@ aux 0 [] l

  let fprint printer fmt l =
    let rec loop = function
      | [] -> ()
      | [ x ] -> Format.fprintf fmt "%a" (fun _ -> printer) x
      | x :: xs ->
          Format.fprintf fmt "%a; " (fun _ -> printer) x;
          loop xs
    in
    Format.fprintf fmt "[";
    loop l;
    Format.fprintf fmt "]"
end
