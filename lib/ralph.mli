module type Data = sig
  type t

  val default : t

  val hash_one : t -> int

  val hash_two : int -> int -> int
end

module type T = sig
  type t
  (** The type of Merkle trees *)

  type leaf_t
  (** The type of the data at the leaves *)

  val root_hash : t -> int
  (** Extract the root hash from a Merkle tree *)

  val make : int -> leaf_t -> t
  (** Create a Merkle Tree of given depth, with all elements initialized with 'init'
   *
   * @raise Invalid_argument if depth is less than 1 *)

  val init_list : leaf_t list -> t
  (** Create a Merkle Tree with all the elements in the list.
   *
   *  If the list length is not a power of 2, this will pad it with G.neutral elements until it is before creating the tree
   *  @raise Invalid_argument if the list is smaller than 2 elements *)

  val modify : int -> (leaf_t -> leaf_t) -> t -> t
  (** Modify a data block at given index with modifier function
   *
   *  @raise Invalid_argument if index is out of bounds *)

  val insert : leaf_t -> t -> t
  (** Insert an element into a full tree, resulting in a doubling of its size.
   *  The given element is duplicated in the right half of the tree.*)

  val proof_tree : int -> t -> int list
  (** Create a proof tree of a MerkleTree and an index,
   *  in the form of the list of hashes necessary to reach the root
   *  @raise Invalid_argument if index is out of bounds *)

  val mem : leaf_t -> int list -> int -> bool
  (** Check if a given item is in a Merkle Tree,
   *  given a proof_tree and the root hash
   *  @raise Invalid_argument if index is out of bounds *)
end

module MerkleTree : functor (D : Data) -> T with type leaf_t = D.t
